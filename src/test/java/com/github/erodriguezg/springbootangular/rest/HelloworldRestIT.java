package com.github.erodriguezg.springbootangular.rest;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

public class HelloworldRestIT extends AbstractRestIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void deberiaRetornarMensajeDeSaludo() throws Exception {
        this.mockMvc
                .perform(
                        get("/hello")
                                .header("origin", "localhost")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello world")));
    }

}
