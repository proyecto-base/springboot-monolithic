package com.github.erodriguezg.springbootangular.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.erodriguezg.springbootangular.entities.Genero;

@Repository
public interface GeneroRepository extends JpaRepository<Genero, Integer> {

	List<Genero> findByOrderByNombre();

}
