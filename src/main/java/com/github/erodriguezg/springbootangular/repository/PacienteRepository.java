package com.github.erodriguezg.springbootangular.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.erodriguezg.springbootangular.entities.Paciente;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Integer> {

	Paciente findByIdPaciente(Integer idPaciente);

}
