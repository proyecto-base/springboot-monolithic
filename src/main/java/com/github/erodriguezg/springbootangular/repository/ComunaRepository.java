package com.github.erodriguezg.springbootangular.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.github.erodriguezg.springbootangular.entities.Comuna;

@Repository
public interface ComunaRepository extends JpaRepository<Comuna, Integer> {

//    List<Comuna> findByProvincia(Provincia provincia);

}
