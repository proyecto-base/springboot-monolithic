package com.github.erodriguezg.springbootangular.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.erodriguezg.springbootangular.dto.GuardarPacienteDto;
import com.github.erodriguezg.springbootangular.entities.Paciente;
import com.github.erodriguezg.springbootangular.services.PacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteRest {

	@Autowired
	private PacienteService pacienteService;

	@GetMapping("/paciente/{idPaciente}")
	public Paciente traerPacientePorId(@PathVariable Integer idPaciente) {
		return this.pacienteService.traerPorId(idPaciente);
	}

	@GetMapping("/traer_pacientes")
	public List<Paciente> traerTodos() {
		return this.pacienteService.traerTodos();
	}

	@PutMapping("/guardar_paciente")
	public void guardarPaciente(@RequestBody @Valid GuardarPacienteDto dto, BindingResult bindResult) {
		if (bindResult.hasErrors()) {
            throw new IllegalArgumentException(bindResult.getAllErrors().toString());
        }

        //mapping para evitar vulnerabilidad de llenado entidad por post
        Paciente paciente = new Paciente();
        BeanUtils.copyProperties(dto, paciente);

		this.pacienteService.guardar(paciente);		
	}
	
	@DeleteMapping("/eliminar/{idPaciente}")
	public void eliminarPaciente(@PathVariable Integer idPaciente) {
		this.pacienteService.eliminar(idPaciente);
	}

}
