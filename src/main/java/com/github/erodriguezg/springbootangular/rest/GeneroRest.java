package com.github.erodriguezg.springbootangular.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.erodriguezg.springbootangular.entities.Genero;
import com.github.erodriguezg.springbootangular.services.GeneroService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/genero")
public class GeneroRest {
	
	@Autowired
	private static final Logger log = LoggerFactory.getLogger(GeneroRest.class);
	
	@Autowired
	private GeneroService generoService; 
	
	@ApiOperation(value = "", notes = "")
	@GetMapping("/v1/generos")
	public List<Genero> traerTodos(){
		log.debug("-> traerTodos");
		return this.generoService.traerTodos(); 
	}

}
