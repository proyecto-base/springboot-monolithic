package com.github.erodriguezg.springbootangular.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "paciente")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString
public class Paciente implements Serializable {

	@Id
	private Integer idPaciente;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_genero")
	private Genero genero;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_comuna")
	private Comuna comuna;
	
	@Column(name = "nombres")
	private String nombres;

	@Column(name = "fechanacimiento")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@Column(name = "direccion")
	private String direccion;

	@Column(name = "telefono")
	private String telefono;

	@Email
	@Column(name = "email")
	private String email;
	
	@Column(name = "departamento")
	private String departamento;
	
	@Column(name = "run")
	private Integer run;
	
	@Column(name = "eliminado")
	private boolean eliminado;

}
