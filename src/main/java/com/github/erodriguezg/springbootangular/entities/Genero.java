package com.github.erodriguezg.springbootangular.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "genero")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString
public class Genero implements Serializable {

	@Id
	@Column(name = "id_genero")
	@EqualsAndHashCode.Include
	private Integer id;
	
	@Column(name = "nombre")
	private String nombre;
}
