package com.github.erodriguezg.springbootangular.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "comuna")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class Comuna implements Serializable {

	private static final long serialVersionUID = 6876833168788473163L;

	@Id
	@Column(name = "id_comuna")
	@EqualsAndHashCode.Include
	private Integer id;

	@Size(max = 100)
	@Column(name = "nombre")
	private String nombre;

}
