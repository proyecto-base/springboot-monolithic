package com.github.erodriguezg.springbootangular.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.erodriguezg.springbootangular.entities.Genero;
import com.github.erodriguezg.springbootangular.repository.GeneroRepository;

@Service
@Transactional(readOnly = true)
public class GeneroService {
	
	@Autowired
	private GeneroRepository generoRepository;

	public List<Genero> traerTodos() {
		return this.generoRepository.findByOrderByNombre();
	}

}
