package com.github.erodriguezg.springbootangular.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.erodriguezg.springbootangular.entities.Paciente;
import com.github.erodriguezg.springbootangular.repository.PacienteRepository;

@Service
@Transactional(readOnly = true)
public class PacienteService {
	
	@Autowired
	private PacienteRepository pacienteRepository;

	public Paciente traerPorId(Integer idPaciente) {
		return this.pacienteRepository.findByIdPaciente(idPaciente);
	}

	public List<Paciente> traerTodos() {
		return this.pacienteRepository.findAll();
	}

	@Transactional(readOnly = false)
	public void guardar(Paciente pacienteParam) {
		this.pacienteRepository.save(pacienteParam);
	}

	public void eliminar(Integer idPaciente) {
		var pacienteExistente = this.pacienteRepository.findById(idPaciente);
		if (!pacienteExistente.isPresent()) {
			return;
		}
		var paciente = pacienteExistente.get();
		this.pacienteRepository.delete(paciente);
	}

}
