package com.github.erodriguezg.springbootangular.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.github.erodriguezg.springbootangular.entities.Comuna;
import com.github.erodriguezg.springbootangular.entities.Genero;

import lombok.Data;

@Data
public class GuardarPacienteDto implements Serializable {

	private Integer idPaciente;

	private Integer run;

	private String nombres;

	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;

	private Genero genero;

	private String direccion;

	private Comuna comuna;

	private String telefono;

	private String email;
	
	private String departamento;
	

}
